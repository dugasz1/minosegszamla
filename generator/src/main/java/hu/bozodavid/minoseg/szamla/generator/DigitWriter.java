package hu.bozodavid.minoseg.szamla.generator;

import hu.bozodavid.minoseg.szamla.common.Digit;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class DigitWriter {
    private File file;
    private StringBuilder[] lineBuilders;

    public DigitWriter(File file, int digitHeight) {
        this.file = file;
        this.lineBuilders = new StringBuilder[digitHeight];

        for (int i = 0; i < lineBuilders.length; i++) {
            lineBuilders[i] = new StringBuilder();
        }
    }

    public void writeDigit(Digit digit) {
        List<String> data = digit.getDataMatrix();
        for (int i = 0; i < data.size(); i++) {
            lineBuilders[i].append(data.get(i));
        }
    }

    public void writeDigitLine(List<Digit> digits){
        for (Digit digit : digits) {
            writeDigit(digit);
        }
        newLine();
    }

    /**
     * Writes out buffers
     */
    public void newLine() {
        String lineSeparator = System.getProperty("line.separator");
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            for (StringBuilder lineBuilder: lineBuilders) {
                fileWriter.write(lineBuilder.toString());
                fileWriter.write(lineSeparator);
                lineBuilder.setLength(0);
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
