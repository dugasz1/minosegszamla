package hu.bozodavid.minoseg.szamla.generator;

import hu.bozodavid.minoseg.szamla.common.DigitCollection;
import hu.bozodavid.minoseg.szamla.common.DigitFactory;

import java.io.File;
import java.io.IOException;


public class Generator {
    public static void main (String args[]){
        DigitCollection digits = DigitFactory.getAvaibleDigits();
/*
        File out = new File("output.txt");

        DigitWriter digitWriter = new DigitWriter(out, 3);
        digitWriter.writeDigitLine(digits.getDigits());
        digitWriter.writeDigitLine(digits.getDigits());
*/


        File out = new File("output.txt");
        File ref = new File("ref.txt");

        DigitGenerator digitGenerator = new DigitGenerator(out, ref);
        try {
            digitGenerator.generate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
