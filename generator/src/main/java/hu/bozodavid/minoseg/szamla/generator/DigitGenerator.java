package hu.bozodavid.minoseg.szamla.generator;

import hu.bozodavid.minoseg.szamla.common.Digit;
import hu.bozodavid.minoseg.szamla.common.DigitCollection;
import hu.bozodavid.minoseg.szamla.common.DigitFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DigitGenerator {
    private File outputFile;
    private File referenceFile;
    private DigitCollection digits;
    private Digit fakeDigit;

    public DigitGenerator(File outputFile, File referenceFile) {
        this.outputFile = outputFile;
        this.referenceFile = referenceFile;
        this.digits = DigitFactory.getAvaibleDigits();
        List<String> lines = new ArrayList<>();
        lines.add("|||");
        lines.add("|||");
        lines.add("|||");
        this.fakeDigit = new Digit(lines, "?");
    }

    public void generate() throws IOException {
        DigitWriter outputWriter = new DigitWriter(outputFile, 3);
        FileWriter refWriter = new FileWriter(referenceFile, true);

        for (int i = 0; i < 100; i++) {
            int[] numbers = getRandomNineNumber();
            boolean isAccNumber = isAccountNumber(numbers);
            boolean isContainsUnknown = isContainsUnknownDigit(numbers);
            List<Digit> digitsToWrite = new ArrayList<>(9);
            StringBuilder refNumber = new StringBuilder();
            for (int j = 0; j < numbers.length; j++) {
                Digit digit;
                if(numbers[j] == 10){
                    digit = fakeDigit;
                } else {
                    digit = digits.getDigits().get(numbers[j]);
                }
                digitsToWrite.add(digit);
                refNumber.append(digit.getDigit());
            }

            if(isContainsUnknown){
                refNumber.append(" ILL");
            }else{
                if(!isAccNumber){
                    refNumber.append(" ERR");
                }
            }


            outputWriter.writeDigitLine(digitsToWrite);
            refWriter.write(refNumber.toString());
            refWriter.write(System.getProperty("line.separator"));

        }
        refWriter.flush();
        refWriter.close();
    }

    public boolean isContainsUnknownDigit(int[] numbers){
        for (int number:numbers) {
            if(number == 10)
                return true;
        }
        return false;
    }

    public boolean isAccountNumber (int[] numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] == 10)
                break;
            sum += (i + 1 ) * numbers[i];
        }
        if (sum % 11 == 0){
            return true;
        }
        return false;
    }

    public int[] getRandomNineNumber(){
        Random random = new Random();
        int[] numbers = new int[9];
        for (int i = 0; i < 9; i++) {
            numbers[i] = random.nextInt(11);
        }

        return numbers;
    }
}
