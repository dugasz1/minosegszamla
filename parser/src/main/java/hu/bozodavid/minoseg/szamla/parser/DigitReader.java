package hu.bozodavid.minoseg.szamla.parser;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DigitReader implements Iterable<List<List<String>>> {
    private final File file;
    private int digitHeight;
    private int digitWidth;
    private List<String> linesBuffers;

    public DigitReader(File file, int digitHeight, int digitWidth) throws IOException {
        this.file = file;
        this.digitHeight = digitHeight;
        this.digitWidth = digitWidth;
        linesBuffers = Files.readAllLines(file.toPath());
    }

    @Override
    public Iterator<List<List<String>>> iterator() {
        return new DigitReaderIterator(linesBuffers);
    }

    private class DigitReaderIterator implements Iterator<List<List<String>>> {
        private List<String> linesBuffer;
        private int row;
        private int col;

        public DigitReaderIterator(List<String> linesBuffers) {
            this.linesBuffer = linesBuffers;
            row = 0;
            col = 0;
        }


        public boolean isPosOutOfMatrix(int[] pos){
            int rowPos = pos[0];
            int colPos = pos[1];

            if(rowPos >= linesBuffer.size() && rowPos + digitHeight - 1 >= linesBuffer.size() ){
                return true;
            }

            for (int i = 0; i < digitHeight; i++) {
                if(colPos >= linesBuffer.get(rowPos + i).length()){
                    return true;
                }
            }

            return false;
        }

        /**
         * Calculates the next position of a digit
         * @return [0] = row , [1] = col
         */
        private int[] nextPosition () {
            if(col + digitWidth >= linesBuffer.get(row).length()){
                //Need to jump to next row
                int[] nextPos = new int[2];
                nextPos[0] = row + digitHeight;
                nextPos[1] = 0;
                return nextPos;
            } else {
                int[] nextPos = new int[2];
                nextPos[0] = row;
                nextPos[1] = col + digitWidth;
                return nextPos;
            }
        }

        public boolean hasNextInLine(){
            if(col + digitWidth >= linesBuffer.get(row).length()){
                return false;
            }

            return true;
        }

        @Override
        public boolean hasNext() {
            int[] nextPos = new int[2];
            nextPos[0] = row;
            nextPos[1] = col;
            if(isPosOutOfMatrix(nextPos)){
                return false;
            } else {
                return true;
            }
        }

        @Override
        public List<List<String>> next() {
            List<List<String>> dataMatrixes = new ArrayList<>();

            do {
                List<String> dataMatrix = new ArrayList<>();

                for (int j = 0; j < digitHeight; j++) {
                    String substring = linesBuffer.get(row + j).substring(col, col + digitHeight);
                    dataMatrix.add(substring);
                }

                boolean hasNextInLine = hasNextInLine();
                int[] nextPos = nextPosition();
                row = nextPos[0];
                col = nextPos[1];

                dataMatrixes.add(dataMatrix);
                if(!hasNextInLine)
                    break;
            }while (true);

            return dataMatrixes;
        }
    }
}
