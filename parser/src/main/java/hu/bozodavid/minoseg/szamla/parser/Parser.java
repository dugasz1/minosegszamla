package hu.bozodavid.minoseg.szamla.parser;

import hu.bozodavid.minoseg.szamla.common.Digit;
import hu.bozodavid.minoseg.szamla.common.DigitFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class Parser {
    public static void main(String args[]){
        /*try {
            DigitReader reader = new DigitReader(new File("output.txt"), 3, 3);
            for (List<String> lines :reader) {
                for (String line : lines) {
                    System.out.println(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        try {
            DigitParser parser = new DigitParser(new File("output.txt"), DigitFactory.getAvaibleDigits());
            File file = new File("parsed.txt");

            PrintWriter printWriter = new PrintWriter(file);

            for (List<Digit> lineOfDigits : parser) {
                boolean wasUnknownDigit = false;
                int sum = 0;

                for (int i = 0; i < lineOfDigits.size(); i++) {
                    Digit digit = lineOfDigits.get(i);
                    if(digit != null){
                        printWriter.print(digit.getDigit());
                        System.out.print(digit.getDigit());
                        int digitInt = Integer.parseInt(digit.getDigit());
                        sum+= (i+1) * digitInt;
                    } else {
                        System.out.print("?");
                        printWriter.print("?");
                        wasUnknownDigit = true;
                    }
                }

                if(wasUnknownDigit){
                    System.out.print(" ILL");
                    printWriter.print(" ILL");
                } else {
                    if(sum%11 != 0){
                        System.out.print(" ERR");
                        printWriter.print(" ERR");
                    }
                }

                System.out.println("");
                printWriter.println("");

            }
            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
