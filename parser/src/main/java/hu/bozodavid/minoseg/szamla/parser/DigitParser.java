package hu.bozodavid.minoseg.szamla.parser;

import hu.bozodavid.minoseg.szamla.common.Digit;
import hu.bozodavid.minoseg.szamla.common.DigitCollection;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DigitParser implements Iterable<List<Digit>> {
    private File file;
    private DigitCollection digits;

    public DigitParser(File file, DigitCollection digits) {
        this.file = file;
        this.digits = digits;
    }

    @Override
    public Iterator<List<Digit>> iterator() {
        try {
            return new DigitParserIterator(new DigitReader(file, 3,3), digits);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class DigitParserIterator implements Iterator<List<Digit>> {
        private DigitReader digitReader;
        Iterator<List<List<String>>> dataMatrixIterator;
        private DigitCollection digits;

        public DigitParserIterator(DigitReader digitReader, DigitCollection digits) {
            this.digitReader = digitReader;
            this.digits = digits;
            this.dataMatrixIterator = digitReader.iterator();
        }

        private StringBuilder stringListToBuilder(List<String> strings){
            StringBuilder stringBuilder = new StringBuilder(strings.size());
            for (String string:strings) {
                stringBuilder.append(string);
            }
            return stringBuilder;
        }

        public Digit findDigit(List<String> dataMatrix){
            Digit foundDigit = null;
            for (Digit digit: digits.getDigits()) {
                if(dataMatrix.equals(digit.getDataMatrix())){
                    foundDigit = digit;
                    break;
                }
            }
            return foundDigit;
        }

        @Override
        public boolean hasNext() {
            return dataMatrixIterator.hasNext();
        }

        @Override
        public List<Digit> next() {
            List<List<String>> lineOfDataMatrix = dataMatrixIterator.next();

            List<Digit> parsedDigits = new ArrayList<>(lineOfDataMatrix.size());

            for (List<String> dataMatrix:lineOfDataMatrix) {

                Digit foundDigit = findDigit(dataMatrix);
                parsedDigits.add(foundDigit);

            }


            return parsedDigits;
        }
    }
}
