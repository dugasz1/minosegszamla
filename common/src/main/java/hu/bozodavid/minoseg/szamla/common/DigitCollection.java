package hu.bozodavid.minoseg.szamla.common;

import java.awt.geom.Point2D;
import java.util.List;

public class DigitCollection {
    private List<Digit> digits;
    private Point2D size;

    public DigitCollection(List<Digit> digits) {
        this.digits = digits;
    }

    public List<Digit> getDigits() {
        return digits;
    }
}
