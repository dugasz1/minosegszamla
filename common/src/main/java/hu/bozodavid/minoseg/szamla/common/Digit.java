package hu.bozodavid.minoseg.szamla.common;

import java.awt.geom.Point2D;
import java.util.List;

/**
 * Structure to hold data for number recognition.
 */
public class Digit {
    private List<String> dataMatrix;
    private StringBuilder linearData;
    private String digit;

    public Digit(List<String> dataMatrix, String digit) {
        this.dataMatrix = dataMatrix;
        this.linearData = new StringBuilder();
        for (String line : dataMatrix) {
            linearData.append(line);
        }
        this.digit = digit;
    }

    public List<String> getDataMatrix() {
        return dataMatrix;
    }

    public String getDigit() {
        return digit;
    }

    public StringBuilder getLinearData() {
        return linearData;
    }

    public int getWidth(){
        return 0;
    }

    @Override
    public String toString(){
        return getDigit();
    }
}