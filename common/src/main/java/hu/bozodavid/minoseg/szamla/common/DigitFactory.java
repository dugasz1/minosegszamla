package hu.bozodavid.minoseg.szamla.common;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class DigitFactory {

    private static DigitCollection avaibleDigits;

    public static DigitCollection getAvaibleDigits(){
        return getAvaibleDigits(false);
    }

    public static DigitCollection getAvaibleDigits(boolean forceReload){
        if(avaibleDigits == null || forceReload == true){
            List<Digit> digits = new ArrayList<>();
            for (File f: getResourceFolderFiles("digits")) {
                System.out.println("Loading file: " + f.toString());
                Digit digit = loadDigit(f);
                digits.add(digit);
            }
            avaibleDigits = new DigitCollection(digits);
            return avaibleDigits;
        }
        return avaibleDigits;
    }

    private static File[] getResourceFolderFiles (String folder) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource(folder);
        String path = url.getPath();
        File[] files = new File(path).listFiles();
        return files;
    }

    private static Digit loadDigit(File file){
        List<String> lines;
        try {
            lines = Files.readAllLines(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (NumberFormatException e){
            System.err.println("Can't load digit: " + file.getName() + ". First line must be a decimal.");
            return null;
        }

        String digit = lines.get(0);
        List<String> dataMatrix = lines.subList(1, lines.size());
        if(!isConstantWidth(dataMatrix)){
            System.err.println("Digit " + file.getName() + " not constant width.");
            //correctConstantWidth(dataMatrix);
        }

        return new Digit(dataMatrix, digit);
    }

    private static void correctConstantWidth(List<String> dataMatrix) {
        throw new NotImplementedException();
    }

    private static boolean isConstantWidth(List<String> lines){
        int width = lines.get(0).length();
        for (String line: lines) {
            if(line.length() != width)
                return false;
        }
        return true;
    }
}
